class ParticipantsController < ApplicationController
  def show
  	@participant = Participant.find(params[:id])
  end

  def new
  	@participant = Participant.new
  end

  def create
    @participant = Participant.new(participant_params)
    if @participant.save
      flash[:success] = "Participant created."
      redirect_to @participant
    else
      render 'new'
    end
  end

  def edit
  	@participant = Participant.find(params[:id])
  end

  def update
    @participant = Participant.find(params[:id])
    if @participant.update_attributes(participant_params)
      flash[:success] = "Participant updated."
      redirect_to @participant
    else
      render 'edit'
    end
  end

  def index
  	@participants = Participant.all
  end

  def destroy
    Participant.find(params[:id]).destroy
    flash[:success] = "Participant deleted."
    redirect_to participants_url
  end

  private

    def participant_params
      params.require(:participant).permit(:name, :email, :company, :role, :city)
    end
end
