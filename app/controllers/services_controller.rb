class ServicesController < ApplicationController
  def show
  	@service = Service.find(params[:id])
  end

  def new
  	@service = Service.new
  end

  def create
    @service = Service.new(service_params)
    if @service.save
      flash[:success] = "Service created."
      redirect_to @service
    else
      render 'new'
    end
  end

  def edit
  	@service = Service.find(params[:id])
  end

  def update
    @service = Service.find(params[:id])
    if @service.update_attributes(service_params)
      flash[:success] = "Service updated."
      redirect_to @service
    else
      render 'edit'
    end
  end

  def index
  	@services = Service.all
  end

  def destroy
    service.find(params[:id]).destroy
    flash[:success] = "Service deleted."
    redirect_to services_url
  end

  private

    def service_params
      params.require(:service).permit(:name)
    end
end
