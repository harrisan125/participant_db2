class Participant < ActiveRecord::Base
  has_many :experiences
  has_many :services, through: :experiences
  has_many :sessions
  has_many :studies, through: :sessions
  before_save { self.email = email.downcase }
  validates :name, presence: true, length: { maximum: 100 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
end
