class Study < ActiveRecord::Base
  has_many :sessions
  has_many :participants, through: :sessions
  has_many :focus
  has_many :services, through: :focus
end
