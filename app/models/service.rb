class Service < ActiveRecord::Base
  has_many :experiences
  has_many :participants, through: :experiences
  has_many :focus
  has_many :studies, through: :focus
end
