class Session < ActiveRecord::Base
  belongs_to :participant
  belongs_to :study
end
