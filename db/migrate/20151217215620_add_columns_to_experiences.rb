class AddColumnsToExperiences < ActiveRecord::Migration
  def change
  	add_column :experiences, :participant_id, :integer
    add_column :experiences, :service_id, :integer
  end
end
