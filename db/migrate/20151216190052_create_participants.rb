class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.string :name
      t.string :email
      t.string :company
      t.string :role
      t.string :city

      t.timestamps null: false
    end
  end
end
