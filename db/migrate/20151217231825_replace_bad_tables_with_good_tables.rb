class ReplaceBadTablesWithGoodTables < ActiveRecord::Migration
  def change
  	add_column :focus, :study_id, :integer
    add_column :focus, :service_id, :integer
    add_column :sessions, :participant_id, :integer
    add_column :sessions, :study_id, :integer
  end
end
